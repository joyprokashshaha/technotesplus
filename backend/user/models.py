from datetime import datetime

from django.db import models
from django.contrib.auth.models import BaseUserManager, AbstractBaseUser, PermissionsMixin, Group
from django.utils import timezone
from django.utils.translation import gettext_lazy as _

from user.managers import UserManager


class User(AbstractBaseUser, PermissionsMixin):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=20)
    username = models.CharField(max_length=30, unique=True)
    email = models.EmailField(verbose_name='email address', max_length=255, unique=True)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    is_superuser = models.BooleanField(default=False)
    date_joined = models.DateTimeField(default=timezone.now)
    # date_joined = models.DateTimeField(_('date joined'), blank=True, null=True)
    date_updated = models.DateTimeField(null=True, blank=True)

    # Tells Django that the UserManager class defined above should manage
    # objects of this type.
    objects = UserManager()

    USERNAME_FIELD = 'username'
    EMAIL_FIELD = 'email'
    REQUIRED_FIELDS = ['email']

    def __str__(self):
        return self.username

    class Meta:
        ordering = ['id']
        db_table = 'auth_user'


class Log(models.Model):
    user = models.ForeignKey(User, null=True, on_delete=models.CASCADE)
    component = models.CharField(max_length=30)
    login_date = models.DateTimeField(null=True)
    logout_date = models.DateTimeField(null=True)
    date_time = models.DateTimeField()
    ip = models.GenericIPAddressField()

    class Meta:
        db_table = 'login'