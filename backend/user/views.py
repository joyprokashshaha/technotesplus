from datetime import datetime

from django.http import HttpResponse
from django.shortcuts import render
from django.utils import timezone

from rest_framework import status
from rest_framework.generics import CreateAPIView, RetrieveAPIView
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework_jwt.authentication import JSONWebTokenAuthentication

from user.models import User, Log
from user.serializers import UserLoginSerializer, UserSerializer, UserRegistrationSerializer, \
    UserPasswordChangeSerializer

# Logger
import technoteplus.settings, logging.config
log = technoteplus.settings.log_setting()
logging.config.dictConfig(log)
mylog = logging.getLogger('user')


# Create your views here.


class IndexView(APIView):
    permission_classes = [AllowAny,]

    def get(self, request):
        response = 'Hello User'
        return Response(response)


class UserRegistrationView(CreateAPIView):
    permission_classes = (AllowAny,)
    serializer_class = UserRegistrationSerializer

    def post(self, request, format=None):
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            serializer.save()
            mylog.info(request.data)
            content = {
                'success': True,
                'statuscode': status.HTTP_201_CREATED,
                'message': 'User is created Successfully',
                'Errors': '',
            }
            return Response(content)
        else:
            print(serializer.errors)
            mylog.error(serializer.errors)
            response = {
                'success': False,
                'statuscode': status.HTTP_400_BAD_REQUEST,
                'message': "Error! Couldn't create new User!",
                'Errors': serializer.errors
            }
            return Response(response)


class UserLoginView(RetrieveAPIView):
    permission_classes = (AllowAny,)
    serializer_class = UserLoginSerializer

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        userid = User.objects.get(username=request.data['username'])
        print('UserID: ', userid.id)

        mylog.info(request.data)
        Log.objects.create(
            user_id=userid.id,
            date_time=timezone.now(),
            login_date=timezone.now(),
            component='LoginUser',
            ip=request.META.get('REMOTE_ADDR')
            # ip=request.META.get('HTTP_X_REAL_IP')
        )
        response = {
            'success': 'True',
            'statuscode': status.HTTP_200_OK,
            'message': 'User logged in  successfully',
            'userid': userid.id,
            'username': request.data['username'],
            'token': serializer.data['token'],
            # 'url': url,
            'Errors': ''
        }
        status_code = status.HTTP_200_OK

        return Response(response, status=status_code)


class UerLogOutView(RetrieveAPIView):
    model = Log

    def post(self, request):
        try:
            user_log = self.model.objects.filter(user_id=request.data['id']).last()
            user_log.logout_date = datetime.now()
            user_log.save()
            response = {
                'success': True,
                'statuscode': status.HTTP_200_OK,
                'message': 'user Successfully',
                'user': '',
                'Errors': ''
            }
            return Response(response)
        except Exception as e:
            print(e)
            response = {
                'success': False,
                'statuscode': status.HTTP_400_BAD_REQUEST,
                'message': 'user does not exists',
                'menu': 0,
                'Errors': ''
            }
            return Response(response)


class UserProfileUpdateView(APIView):
    permission_classes = (IsAuthenticated,)
    authentication_class = JSONWebTokenAuthentication

    def get_object(self, pk):
        try:
            return User.objects.get(pk=pk)
        except User.DoesNotExist:
            response = {
                'success': False,
                'statuscode': status.HTTP_400_BAD_REQUEST,
                'message': 'User does not exists',
                'Errors': ''
            }
            return Response(response)

    def get(self, request, pk, format=None):
        user = self.get_object(pk)
        serializer = UserSerializer(user)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        user = self.get_object(pk)
        serializer = UserSerializer(user, data=request.data)
        if serializer.is_valid():
            serializer.save()
            user.date_updated = timezone.now()
            user.save()
            mylog.info(request.data)
            content = {
                'success': True,
                'statuscode': status.HTTP_200_OK,
                'message': 'User Updated Successfully',
                'Errors': ''
            }
            return Response(content)
        response = {
            'success': False,
            'statuscode': status.HTTP_400_BAD_REQUEST,
            'message': 'User does not exists',
            'Errors': serializer.errors
        }
        mylog.error(serializer.errors)

        return Response(response)

    def delete(self, request, pk, format=None):
        user = self.get_object(pk)
        try:
            user.delete()
            content = {
                'success': True,
                'statuscode': status.HTTP_200_OK,
                'message': 'User Deleted Successfully',
                'Errors': ''
            }
            return Response(content)

        except Exception as e:
            response = {
                'success': False,
                'statuscode': status.HTTP_400_BAD_REQUEST,
                'message': 'User does not exists',
                'Errors': ''
            }
            return Response(response)


class UserPasswordChangeView(APIView):
    permission_classes = [IsAuthenticated]

    def patch(self, request):
        user = User.objects.get(pk=request.data['id'])
        serialized = UserPasswordChangeSerializer(user, data=request.data)
        if serialized.is_valid():
            serialized.save()
            user.set_password(serialized.data.get('password'))
            user.save()
            response = {
                'success': True,
                'statuscode': status.HTTP_200_OK,
                'message': 'Password Update Successfully',
                'Errors': ''
            }
            return Response(response)
        else:
            response = {
                'success': False,
                'statuscode': status.HTTP_400_BAD_REQUEST,
                'message': 'Password does not exists',
                'Errors': serialized.errors
            }
            return Response(response)


class UserListCreateView(APIView):
    permission_classes = (IsAuthenticated,)
    # authentication_class = JSONWebTokenAuthentication
    # print(authentication_class)

    def get(self, request, format=None):
        user = User.objects.all()
        serializer = UserSerializer(user, many=True)
        try:
            response = {
                'success': True,
                'statuscode': status.HTTP_200_OK,
                'message': 'user Successfully',
                'users': serializer.data,
                'Errors': ''
            }
            return Response(response)
        except Exception as e:
            print(e)
            response = {
                'success': False,
                'statuscode': status.HTTP_400_BAD_REQUEST,
                'message': 'user does not exists',
                'menu': 0,
                'Errors': serializer.errors
            }
            return Response(response)

    def post(self, request, format=None):
        serializer = UserRegistrationSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            mylog.info(request.data)

            content = {
                'success': True,
                'statuscode': status.HTTP_201_CREATED,
                'message': 'User is created Successfully',
                'Errors': '',
            }
            return Response(content)
        else:
            print(serializer.errors)
            mylog.error(serializer.errors)
            response = {
                'success': False,
                'statuscode': status.HTTP_400_BAD_REQUEST,
                'message': "Error! Couldn't create new User!",
                'Errors': serializer.errors
            }
            return Response(response)

