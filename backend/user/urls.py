from django.urls import path

from .views import *

app_name = 'user'

urlpatterns = [
    path('registration', UserRegistrationView.as_view(), name='UserRegistrationView'),
    path('login', UserLoginView.as_view(), name='UserLoginView'),
    path('logout', UerLogOutView.as_view(), name='UerLogOutView'),
    path('profile/<pk>', UserProfileUpdateView.as_view(), name='UserProfileUpdateView'),
    path('password', UserPasswordChangeView.as_view(), name='UserPasswordChangeView'),
    path('user', UserListCreateView.as_view(), name='UserListCreateView'),
]