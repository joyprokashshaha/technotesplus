from django.shortcuts import render
from django.utils import timezone
from rest_framework import status

from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from rest_framework_jwt.authentication import JSONWebTokenAuthentication

from notes.models import Note, Share, Tag
from notes.serializers import NoteSerializer, ShareSerializer, TagSerializer, NoteTagSerializer

# Logger
import technoteplus.settings, logging.config
from user.models import User

log = technoteplus.settings.log_setting()
logging.config.dictConfig(log)
mylog = logging.getLogger('notes')

# Email
from django.core.mail import send_mail

# Create your views here.


class IndexView(APIView):
    permission_classes = [AllowAny,]

    def get(self, request):
        response = 'Hello Note'
        return Response(response)


def generic_getResponse(model, ModelSerializer, ClassName, pk):
    try:
        modelObject = model.objects.get(id=pk)
        serializer = ModelSerializer(modelObject)
        response = {
            'success': True,
            'statuscode': status.HTTP_200_OK,
            "message": ClassName,
            'data': serializer.data,
            'Errors': ''
        }
        return response

    except model.DoesNotExist:
        message = ClassName + " does not exists"
        response = {
            'success': False,
            'statuscode': status.HTTP_400_BAD_REQUEST,
            'message': message,
            'Errors': ''
        }
        return response


def generic_postResponse(ModelSerializer, request, ClassName):
    try:
        serializer = ModelSerializer(data=request.data)
        if serializer.is_valid() and len(request.data) > 0:
            # print(serializer.data)
            serializer.save(user=request.user, created_date=timezone.now())
            mylog.info(request.data)
            message = ClassName + " is created Successfully"
            content = {
                'success': True,
                'statuscode': status.HTTP_200_OK,
                'message': message,
                'Errors': ''
            }
            return content

        message = "Validation Error"
        response = {
            'success': False,
            'statuscode': status.HTTP_400_BAD_REQUEST,
            'message': message,
            'Errors': serializer.errors
        }
        mylog.error(serializer.errors)

        return response

    except:
        # print(serializer.errors)
        message = "Error! Couldn't create new " + ClassName
        response = {
            'success': False,
            'statuscode': status.HTTP_400_BAD_REQUEST,
            'message': message,
            'Errors': serializer.errors
        }
        mylog.error(serializer.errors)

        return response


def generic_putResponse(request, model, pk, ModelSerializer, ClassName):
    try:
        modelObject = model.objects.get(pk=pk)
        serializer = ModelSerializer(modelObject, data=request.data)

        if serializer.is_valid():
            serializer.save(user=request.user, updated_date=timezone.now())
            mylog.info(request.data)
            message = ClassName + " Updated Successfully"
            response = {
                'success': True,
                'statuscode': status.HTTP_200_OK,
                'message': message,
                'Errors': ''
            }
            return response
        message = "Validation error"
        response = {
            'success': False,
            'statuscode': status.HTTP_400_BAD_REQUEST,
            'message': message,
            'Errors': serializer.errors
        }
        mylog.error(serializer.errors)
        return response

    except model.DoesNotExist:
        message = ClassName + ' does not exists'
        response = {
            'success': False,
            'statuscode': status.HTTP_400_BAD_REQUEST,
            'message': message,
            'Errors': ''
        }
        return response


def generic_deleteResponse(request, model, pk, ClassName):
    try:
        ModelObject = model.objects.get(pk=pk)
        ModelObject.delete()
        message = ClassName + " Deleted Successfully"
        response = {
            'success': True,
            'statuscode': status.HTTP_200_OK,
            'message': message,
            'Errors': ''
        }
        return response

    except model.DoesNotExist:
        message = ClassName + " does not exists"
        response = {
            'success': False,
            'statuscode': status.HTTP_400_BAD_REQUEST,
            'message': message,
            'Errors': ''
        }
        return response


'''NOTE'''


class NoteListUserBasedView(APIView):
    permission_classes = (IsAuthenticated,)
    authentication_class = JSONWebTokenAuthentication

    def get(self, request, pk, format=None):
        note = Note.objects.all().filter(user_id=pk)
        serializer = NoteSerializer(note, many=True)
        return Response(serializer.data)


class NoteListCreateView(APIView):
    permission_classes = (IsAuthenticated,)
    authentication_class = JSONWebTokenAuthentication

    def get(self, request, format=None):
        # note = Note.objects.filter(user_id=request.data['id'])
        note = Note.objects.all()
        serializer = NoteSerializer(note, many=True)
        return Response(serializer.data)

    def post(self, request, *args, **kwargs):
        return Response(generic_postResponse(ModelSerializer=NoteSerializer,
                                             request=request, ClassName="Note"))


class NoteRetrieveUpdateDeleteView(APIView):
    permission_classes = (IsAuthenticated,)
    authentication_class = JSONWebTokenAuthentication

    def get(self, request, pk, format=None):
        return Response(generic_getResponse(model=Note,
                                            ModelSerializer=NoteSerializer,
                                            ClassName="Note", pk=pk))

    def put(self, request, pk, format=None):
        return Response(generic_putResponse(request=request,
                                            model=Note, pk=pk,
                                            ModelSerializer=NoteSerializer,
                                            ClassName="Note"))

    def delete(self, request, pk, format=None):
        return Response(generic_deleteResponse(request=request,
                                               model=Note, pk=pk,
                                               ClassName="Note"))


'''SHARING'''


class ShareListView(APIView):
    permission_classes = (IsAuthenticated,)
    authentication_class = JSONWebTokenAuthentication

    def get(self, request, pk, format=None):
        user = User.objects.get(pk=pk)
        share = Share.objects.all().filter(user_id=user.id)
        share.update(is_seen=True)
        serializer = ShareSerializer(share, many=True)
        return Response(serializer.data)


class ShareCreateView(APIView):
    permission_classes = (IsAuthenticated,)
    authentication_class = JSONWebTokenAuthentication

    def post(self, request, *args, **kwargs):
        try:
            note = Note.objects.get(pk=request.data['note'])
            user = User.objects.get(pk=request.data['user'])
            if user and note:
                Share.objects.create(note_id=note.id, user_id=user.id, created_date=timezone.now())
                mylog.info('Note is shared Successfully to user' + user.username)

                try:
                    send_mail('Sharing Note', 'You have shared a note named ' + note.text,
                              'joyshaha.iot@gmail.com', [user.email],
                              fail_silently=False)
                except Exception as ex:
                    mylog.info('email not send ' + str(ex))
                    pass

                content = {
                    'success': True,
                    'statuscode': status.HTTP_201_CREATED,
                    'message': 'Note is shared Successfully to user ' + user.username,
                    'Errors': '',
                }
                return Response(content)
            else:
                mylog.error('Error! Could not Shared to User!')
                response = {
                    'success': False,
                    'statuscode': status.HTTP_400_BAD_REQUEST,
                    'message': "Error! Couldn't Shared to User!",
                    'Errors': ''
                }
                return Response(response)
        except Exception as Ex:
            mylog.error(str(Ex))
            response = {
                'success': False,
                'statuscode': status.HTTP_400_BAD_REQUEST,
                'message': str(Ex),
                'Errors': ''
            }
            return Response(response)


'''TAG'''


class TagListCreateView(APIView):
    permission_classes = (IsAuthenticated,)
    authentication_class = JSONWebTokenAuthentication

    def get(self, request, format=None):
        tag = Tag.objects.all()
        serializer = TagSerializer(tag, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = TagSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save(note_id=request.data['note'], created_date=timezone.now())
            mylog.info(request.data)

            content = {
                'success': True,
                'statuscode': status.HTTP_201_CREATED,
                'message': 'Tag is created Successfully',
                'Errors': '',
            }
            return Response(content)
        else:
            print(serializer.errors)
            mylog.error(serializer.errors)
            response = {
                'success': False,
                'statuscode': status.HTTP_400_BAD_REQUEST,
                'message': "Error! Couldn't create Tag!",
                'Errors': serializer.errors
            }
            return Response(response)


class NoteTagListView(APIView):
    permission_classes = (IsAuthenticated,)
    authentication_class = JSONWebTokenAuthentication

    def get(self, request, pk, format=None):
        tag = Tag.objects.all().filter(note_id=pk)
        serializer = TagSerializer(tag, many=True)
        return Response(serializer.data)
