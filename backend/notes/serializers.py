from rest_framework import serializers
# from rest_framework_jwt.settings import api_settings

from notes.models import Note, Share, Tag


# JWT_PAYLOAD_HANDLER = api_settings.JWT_PAYLOAD_HANDLER
# JWT_ENCODE_HANDLER = api_settings.JWT_ENCODE_HANDLER


class NoteSerializer(serializers.ModelSerializer):
    user = serializers.PrimaryKeyRelatedField(read_only=True)

    class Meta:
        model = Note
        fields = '__all__'


class ShareSerializer(serializers.ModelSerializer):
    user = serializers.SerializerMethodField('get_user')
    note = serializers.SerializerMethodField('get_note')

    class Meta:
        model = Share
        # fields = ['id', 'note', 'created_date']
        fields = '__all__'

    def get_note(self, obj):
        try:
            return obj.note.text
        except:
            return None

    def get_user(self, obj):
        try:
            return obj.user.username
        except:
            return None


class TagSerializer(serializers.ModelSerializer):
    note = serializers.SerializerMethodField('get_note')

    class Meta:
        model = Tag
        fields = '__all__'

    def get_note(self, obj):
        try:
            return obj.note.text
        except:
            return None


class NoteTagSerializer(serializers.ModelSerializer):
    # tags = serializers.PrimaryKeyRelatedField(read_only=True)
    tags = TagSerializer(read_only=True)

    class Meta:
        model = Note
        fields = ['text', 'emoji', 'tags']



