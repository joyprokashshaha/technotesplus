
from django.db import models

from user.models import User

# Create your models here.


class Note(models.Model):
    text = models.TextField(max_length=200)
    emoji = models.CharField(max_length=10, blank=True, null=True)
    user = models.ForeignKey(User, null=True, on_delete=models.CASCADE)
    created_date = models.DateTimeField(blank=True, null=True)
    updated_date = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        return self.text

    class Meta:
        ordering = ['id']
        db_table = 'note'


class Tag(models.Model):
    tag = models.CharField(max_length=200)
    note = models.ForeignKey(Note, on_delete=models.CASCADE)
    created_date = models.DateTimeField(blank=True, null=True)
    updated_date = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        return self.tag

    class Meta:
        ordering = ['id']
        db_table = 'tag'


class Share(models.Model):
    note = models.ForeignKey(Note, null=True, on_delete=models.CASCADE)
    user = models.ForeignKey(User, null=True, on_delete=models.CASCADE)
    created_date = models.DateTimeField(blank=True, null=True)
    is_seen = models.BooleanField(default=False)

    class Meta:
        db_table = 'share'