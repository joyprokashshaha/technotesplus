from django.urls import path

from notes.views import IndexView, NoteListCreateView, NoteRetrieveUpdateDeleteView, NoteListUserBasedView, \
    ShareCreateView, ShareListView, TagListCreateView, NoteTagListView

app_name = 'notes'

urlpatterns = [
    # path('', IndexView.as_view(), name='IndexView'),

    # Note
    path('note', NoteListCreateView.as_view(), name='NoteListCreateView'),
    path('note/user/<int:pk>', NoteListUserBasedView.as_view(), name='NoteListUserBasedView'),
    path('note/<int:pk>', NoteRetrieveUpdateDeleteView.as_view(), name='NoteRetrieveUpdateDeleteView'),

    # Share
    path('share/<int:pk>', ShareListView.as_view(), name='ShareListView'),
    # path('note/<int:id>/share/<int:pk>', ShareCreateView.as_view(), name='ShareCreateView'),
    path('share', ShareCreateView.as_view(), name='ShareCreateView'),

    # Tag
    path('tag', TagListCreateView.as_view(), name='TagListCreateView'),
    path('note/tag/<int:pk>', NoteTagListView.as_view(), name='NoteTagListView'),
]