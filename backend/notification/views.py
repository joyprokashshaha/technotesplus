from django.shortcuts import render
from background_task import background

from notes.models import Share

from django.core.mail import send_mail

import technoteplus.settings, logging.config
log = technoteplus.settings.log_setting()
logging.config.dictConfig(log)
mylog = logging.getLogger('notification')

# Create your views here.


@background(schedule=3600)
def notify_user():
    # lookup user by id and send them a message
    print('Scheduler is running')
    is_not_seen = Share.objects.filter(is_seen=False)
    print(is_not_seen)
    for data in is_not_seen:
        try:
            send_mail('Sharing Note', 'You have shared a note named ' + data.note.text,
                      'joyshaha.iot@gmail.com', [data.user.email],
                      fail_silently=False)
        except Exception as ex:
            mylog.info('email not send ' + str(ex))
            pass

# python manage.py process_tasks