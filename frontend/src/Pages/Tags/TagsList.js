import moment from "moment";
import React, { useState, useEffect } from "react";
import Layout from "../../Components/Layout/Layout";
import { httpGeneral } from "../../Utils/Adapters";

function TagsList() {
  const [tags, setTags] = useState([]);
  const fetchTags = () => {
    httpGeneral
      .get("api/tag")
      .then((res) => {
        // console.log(res.data);
        setTags(res.data);
      })
      .catch((err) => console.log(err));
  };

  useEffect(() => {
    fetchTags();
    return () => {
      fetchTags();
    };
  }, []);
  return (
    <div>
      <Layout>
        <div className="row">
          <h4 className="text-muted mb-3 mt-3">Tags</h4>
          {tags.map((d) => (
            <>
              <div className="col-sm-12 col-md-3 col-lg-3">
                <div className="card shadow mb-4" key={d.id}>
                  <div className="card-body">
                    <small className="text-muted m-0 " style={{ fontSize: "12px" }}>
                      {moment(d.created_date).startOf("hour").fromNow()}
                    </small>
                    <h5 className="mb-2">{d.tag}</h5>
                    <h3 className="mb-2">{d.note}</h3>
                  </div>
                </div>
              </div>
            </>
          ))}
        </div>
      </Layout>
    </div>
  );
}

export default TagsList;
