import React, { useEffect, useState } from "react";
import Layout from "../../Components/Layout/Layout";
import { httpGeneral } from "../../Utils/Adapters";
import { getUser } from "../../Utils/UserData";

function Tags(props) {
  const user = getUser();
  const [note, setNote] = useState([]);
  const [noteId, setNoteId] = useState();
  const [tag, setTag] = useState("");

  const AllNote = () => {
    httpGeneral
      .get(`api/note/user/${user.userid}`)
      .then((res) => {
        // console.log(res.data);
        setNote(res.data);
      })
      .catch((err) => console.log(err));
  };

  useEffect(() => {
    AllNote();
    return () => {
      AllNote();
    };
  }, []);

  const handleSubmit = (e) => {
    e.preventDefault();
    const data = {
      note: noteId,
      tag: tag,
    };
    httpGeneral
      .post("api/tag", data)
      .then((res) => {
        // console.log(res.data);
        props.history.push("/home");
      })
      .catch((err) => console.log(err));
  };
  return (
    <div>
      <Layout>
        <h5 className="mb-2 text-muted">Create Tag</h5>
        <input
          type="text"
          className="form-control shadow-sm mb-2"
          placeholder="Write your note"
          onChange={(e) => setTag(e.target.value)}
        />

        <select
          class="form-select form-select-lg mb-3"
          aria-label=".form-select-lg example"
          onChange={(e) => setNoteId(e.target.value)}
        >
          <option selected>Select note</option>
          {note.map((d) => (
            <option value={d.id}>{d.text}</option>
          ))}
        </select>
        <button className="btn btn-primary btn-sm shadow" onClick={handleSubmit}>
          Create Tag
        </button>
      </Layout>
    </div>
  );
}

export default Tags;
