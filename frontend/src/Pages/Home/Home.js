import moment from "moment";
import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import Layout from "../../Components/Layout/Layout";
import { httpGeneral } from "../../Utils/Adapters";
import { getUser } from "../../Utils/UserData";

function Home() {
  const [notes, setNotes] = useState([]);

  const id = getUser();

  const fetchNote = () => {
    httpGeneral
      .get(`api/note/user/${id.userid}`)
      .then((res) => {
        // console.log(res.data);
        setNotes(res.data);
      })
      .catch((err) => console.log(err));
  };

  useEffect(() => {
    fetchNote();
    return () => {
      fetchNote();
    };
  }, []);

  const handleDlt = (id) => {
    httpGeneral
      .delete(`api/note/${id}`)
      .then((res) => {
        console.log(res.data);
        fetchNote();
      })
      .catch((err) => console.log(err));
  };
  return (
    <div>
      <Layout>
        <div className="row">
          <div className="col-sm-12 col-md-12 col-lg-12 mb-2">
            <div className="d-flex justify-content-end">
              <Link to="/note/add">
                <button className="btn btn-primary btn-sm shadow-sm " style={{ marginRight: "6px" }}>
                  <i class="bi bi-plus"></i>
                  Add Note
                </button>
              </Link>
              <Link to="/tag/add">
                <button className="btn btn-secondary btn-sm shadow-sm " style={{ marginRight: "6px" }}>
                  <i class="bi bi-tag"></i>
                  Add Tag
                </button>
              </Link>
            </div>
          </div>

          <h4 className="text-muted mb-3 mt-3">Note</h4>
          {notes.map((d) => (
            <div className="col-sm-12 col-md-3 col-lg-3">
              <Link to={`/note/${d.id}`}>
                <div className="card shadow mb-4" key={d.id}>
                  <div className="card-body">
                    <>
                      <small className="text-muted m-0 " style={{ fontSize: "12px" }}>
                        {moment(d.created_date).startOf("hour").fromNow()}
                      </small>
                      <h5 className="mb-2">{d.text}</h5>
                      <div className="d-flex justify-content-start">
                        <Link to={`/notes/edit/${d.id}`}>
                          <button
                            className="btn btn-secondary btn-sm shadow-sm"
                            data-bs-toggle="tooltip"
                            data-bs-placement="top"
                            title="Edit Note"
                            style={{ marginRight: "6px" }}
                          >
                            <i class="bi bi-pencil-square"></i>
                          </button>
                        </Link>

                        <button
                          className="btn btn-danger btn-sm shadow-sm "
                          data-bs-toggle="tooltip"
                          data-bs-placement="top"
                          title="Delete Note"
                          style={{ marginRight: "6px" }}
                          value={d.id}
                          onClick={(e) => {
                            handleDlt(d.id);
                          }}
                        >
                          <i class="bi bi-trash"></i>
                        </button>
                        <Link to={`/share/${d.id}`}>
                          <button
                            className="btn btn-primary btn-sm shadow-sm "
                            data-bs-toggle="tooltip"
                            data-bs-placement="top"
                            title="Share Note"
                            style={{ marginRight: "6px" }}
                          >
                            <i class="bi bi-share"></i>
                          </button>
                        </Link>
                      </div>
                    </>
                  </div>
                </div>
              </Link>
            </div>
          ))}
        </div>
      </Layout>
    </div>
  );
}

export default Home;
