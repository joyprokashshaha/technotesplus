import React, { useState } from "react";
import { Link } from "react-router-dom";
import { httpLogin } from "../../Utils/Adapters";

function Registration(props) {
  const [loading, setLoading] = useState(false);
  const [fName, setFname] = useState("");
  const [lName, setLname] = useState("");
  const [email, setEmail] = useState("");
  const [username, setUserName] = useState("");
  const [password, setPassword] = useState("");
  const [isActive, setIsActive] = useState(true);

  //Error
  const [emailErr, setEmailErr] = useState("");
  const [usernameErr, setUserNameErr] = useState("");
  const [passError, setPassErr] = useState("");

  const handleSubmit = (e) => {
    e.preventDefault();
    if (isActive === true) {
      const data = {
        first_name: fName,
        last_name: lName,
        email: email,
        username: username,
        password: password,
        is_active: true,
      };
      httpLogin
        .post("api/registration", data)
        .then((res) => {
          console.log(res.data);
          if (res.data.statuscode === 400) {
            if (res.data.Errors.password) {
              setPassErr(res.data.Errors.password[0]);
            } else if (res.data.Errors.email) {
              setEmailErr(res.data.Errors.email[0]);
            } else if (res.data.Errors.username) {
              setUserNameErr(res.data.Errors.username[0]);
            }
          } else if (res.data.statuscode === 201) {
            props.history.push("/");
            setPassErr("");
            setEmailErr("");
            setUserNameErr("");
          }
        })
        .catch((err) => console.log(err));
    }
    setLoading(false);
  };
  return (
    <div>
      <div className="container d-flex justify-content-center align-items-center" style={{ height: "100vh" }}>
        <div className="card shadow" style={{ minWidth: "350px" }}>
          <div className="card-body">
            <form onSubmit={handleSubmit}>
              <h3 className="mb-3 text-center">Registration</h3>
              <div class="form-floating mb-2">
                <input
                  type="text"
                  class="form-control shadow-sm"
                  placeholder="First Name"
                  value={fName}
                  onChange={(e) => setFname(e.target.value)}
                  required
                />
                <label for="floatingPassword">First Name</label>
              </div>
              <div class="form-floating mb-2">
                <input
                  type="text"
                  class="form-control shadow-sm"
                  placeholder="Last Name"
                  value={lName}
                  onChange={(e) => setLname(e.target.value)}
                  required
                />
                <label for="floatingPassword">Last Name</label>
              </div>
              <div class="form-floating mb-2">
                <input
                  type="email"
                  class="form-control shadow-sm"
                  placeholder="Email"
                  value={email}
                  onChange={(e) => setEmail(e.target.value)}
                  required
                />
                <label for="floatingPassword">Email</label>
                {emailErr ? <small className="text-danger">{emailErr}</small> : ""}
              </div>
              <div class="form-floating mb-2">
                <input
                  type="text"
                  class="form-control shadow-sm"
                  placeholder="Username"
                  value={username}
                  onChange={(e) => setUserName(e.target.value)}
                  required
                />
                <label for="floatingPassword">Username</label>
                {usernameErr ? <small className="text-danger">{usernameErr}</small> : ""}
              </div>
              <div class="form-floating mb-2">
                <input
                  type="password"
                  class="form-control shadow-sm"
                  placeholder="Password"
                  value={password}
                  onChange={(e) => setPassword(e.target.value)}
                  required
                />
                <label for="floatingPassword">Password</label>
                {passError ? <small className="text-danger">{passError}</small> : ""}
              </div>
              <div class="form-check form-switch mb-2">
                <label class="form-check-label" for="flexSwitchCheckChecked">
                  Is Active?
                </label>
                <input
                  class="form-check-input"
                  type="checkbox"
                  id="flexSwitchCheckChecked"
                  checked={isActive === true}
                  onChange={(e) => setIsActive(e.target.checked)}
                  required
                />
              </div>

              <div class="d-grid gap-2">
                <button type="submit" className="btn btn-primary btn-sm shadow-sm" disabled={loading}>
                  {loading ? "Register..." : "Register"}
                </button>
              </div>
              <small className="mt-2">
                have an account? <Link to="/">Login</Link>
              </small>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Registration;
