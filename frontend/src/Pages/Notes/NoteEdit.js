import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import Layout from "../../Components/Layout/Layout";
import { httpGeneral } from "../../Utils/Adapters";

function NoteEdit(props) {
  const { id } = useParams();

  const [note, setNote] = useState("");
  const [getNote, setGetNote] = useState("");

  useEffect(() => {
    httpGeneral
      .get(`api/note/${id}`)
      .then((res) => {
        // console.log(res.data);
        setNote(res.data.data.text);
      })
      .catch((err) => console.log(err));
  }, []);

  const handleSubmit = (e) => {
    e.preventDefault();
    httpGeneral
      .put(`api/note/${id}`, { text: note })
      .then((res) => {
        console.log(res.data);
        if (res.data.statuscode === 200) {
          props.history.push("/home");
        }
      })
      .catch((err) => console.log(err));
    setNote("");
  };
  return (
    <div>
      <Layout>
        <div className="row">
          <div className="col-md-8 col-sm-12 m-auto">
            <div className="form-group">
              <input
                type="text"
                className="form-control shadow-sm"
                placeholder="Write your note"
                value={note}
                onChange={(e) => setNote(e.target.value)}
              />
            </div>
            <button className="btn btn-primary shadow-sm mt-2" onClick={handleSubmit}>
              Post
            </button>
          </div>
        </div>
      </Layout>
    </div>
  );
}

export default NoteEdit;
