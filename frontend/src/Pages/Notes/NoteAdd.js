import React, { useState } from "react";
import Layout from "../../Components/Layout/Layout";
import { httpGeneral } from "../../Utils/Adapters";

function NoteAdd(props) {
  const [note, setNote] = useState("");
  const handleSubmit = (e) => {
    e.preventDefault();
    httpGeneral
      .post("api/note", { text: note })
      .then((res) => {
        console.log(res.data);
        if (res.data.statuscode === 200) {
          props.history.push("/home");
        }
      })
      .catch((err) => console.log(err));
    setNote("");
  };
  return (
    <div>
      <Layout>
        <h2 className="mb-2 text-muted">Create New Note</h2>
        <div className="row">
          <div className="col-md-8 col-sm-12 m-auto">
            <div className="form-group">
              <textarea
                type="text"
                className="form-control shadow-sm"
                placeholder="Write your note"
                onChange={(e) => setNote(e.target.value)}
              />
            </div>
            <button className="btn btn-primary shadow-sm mt-2" onClick={handleSubmit}>
              {" "}
              Post
            </button>
          </div>
        </div>
      </Layout>
    </div>
  );
}

export default NoteAdd;
