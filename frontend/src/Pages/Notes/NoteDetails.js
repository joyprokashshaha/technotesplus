import moment from "moment";
import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import Layout from "../../Components/Layout/Layout";
import { httpGeneral } from "../../Utils/Adapters";

function NoteDetails() {
  const { id } = useParams();
  const [note, setNote] = useState("");
  const [tags, setTags] = useState([]);

  const noteData = () => {
    httpGeneral
      .get(`api/note/${id}`)
      .then((res) => {
        // console.log(res.data.data);
        setNote(res.data.data);
      })
      .catch((err) => console.log(err));
  };

  const tagData = () => {
    httpGeneral
      .get(`api/note/tag/${id}`)
      .then((res) => {
        console.log(res.data);
        setTags(res.data);
      })
      .catch((err) => console.log(err));
  };

  useEffect(() => {
    noteData();
    tagData();
    return () => {
      noteData();
      tagData();
    };
  }, []);

  return (
    <div>
      <Layout>
        {/* <div>
          <div className="card shadow">
            <div className="card-body">
            <h5>{note.text}</h5>
              {tags.map((d) => (
                <span class="badge bg-secondary" style={{ marginRight: "10px" }} key={d.id}>
                  {d.tag}
                </span>
              ))}          
            </div>
          </div>
        </div> */}
        <div className="row">
          <h4 className="text-muted mb-3 mt-3">{note.text}</h4>
          {tags.map((d) => (
            <>
              <div className="col-sm-12 col-md-3 col-lg-3">
                <div className="card shadow mb-4" key={d.id}>
                  <div className="card-body">
                    <h3 className="mb-3">{d.tag}</h3>
                    <h5 className="mb-1">{d.note}</h5>
                    <small className="mb-1 " style={{ fontSize: "12px" }}>
                      {moment(d.created_date).startOf("hour").fromNow()}
                    </small>
                  </div>
                </div>
              </div>
            </>
          ))}
        </div>
      </Layout>
    </div>
  );
}

export default NoteDetails;
