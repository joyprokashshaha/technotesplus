import React, { useState, useEffect } from "react";
import Layout from "../../Components/Layout/Layout";
import { httpGeneral } from "../../Utils/Adapters";
import { getUser, removeUserSession } from "../../Utils/UserData";

function Password(props) {
  const changePassword = getUser();
  const [loading, setLoading] = useState(false);
  const [changePass, setPassword] = useState("");

  const edit = (e)=>{
    e.preventDefault();
    const data = {  
      id: changePassword.userid,
      password: changePass,
    }; 
    httpGeneral.patch(`api/password`, data).then((res)=>{
      if(res.data.statuscode===200){
        removeUserSession()
        props.history.push('/')
      }
    }).catch(err=>console.log(err))
  }
//   const [user, setUser] = useState([]);
//   const fetchPassword = () => {
//     httpGeneral
//       .get(`api/password`)
//       .then((res) => {
//         // console.log(res.data);
//         setUser(res.data);
//         setPassword(res.data.password)      
//       })
//       .catch((err) => console.log(err));
//   };

//   useEffect(() => {
//     fetchPassword();
//     return () => {
//       fetchPassword();
//     };
//   }, []);

  return (
    <div>
      <Layout>
      <form onSubmit={edit}>
              <h3 className="mb-3 text-center">Change Password</h3>
              <div class="form-floating mb-2">
                <input
                  type="password"
                  class="form-control shadow-sm"
                  placeholder="Password"
                  value={changePass}
                  onChange={(e) => setPassword(e.target.value)}
                  required
                />
                <label for="floatingPassword">Password</label>
              </div>
              <div class="d-grid gap-2">
                <button type="submit" className="btn btn-primary btn-sm shadow-sm" disabled={loading}>
                  {loading ? "Changing..." : "Submit"}
                </button>
              </div>
            </form>
      </Layout>
    </div>
  );
}

export default Password;
