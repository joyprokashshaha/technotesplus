import React, { useState, useEffect } from "react";
import Layout from "../../Components/Layout/Layout";
import { httpGeneral } from "../../Utils/Adapters";
import { getUser } from "../../Utils/UserData";

function Profile(props) {
  const loginUser = getUser();
  const [loading, setLoading] = useState(false);
  const [fName, setFname] = useState("");
  const [lName, setLname] = useState("");
  const [email, setEmail] = useState("");
  const [username, setUserName] = useState("");
  const [isActive, setIsActive] = useState(false);

  const edit = (e)=>{
    e.preventDefault();
    const data = {
      first_name: fName,
      last_name: lName,
      email: email,
      username: username,
      is_active: true,
    }; 
    httpGeneral.put(`api/profile/${loginUser.userid}`,data).then((res)=>{
      if(res.data.statuscode===200){
        props.history.push('/home')
      }
    }).catch(err=>console.log(err))
  }
  const [user, setUser] = useState([]);
  const fetchProfile = () => {
    httpGeneral
      .get(`api/profile/${loginUser.userid}`)
      .then((res) => {
        // console.log(res.data);
        setUser(res.data);
        setFname(res.data.first_name)
        setLname(res.data.last_name)
        setEmail(res.data.email)
        setUserName(res.data.username)
        setIsActive(res.data.is_active)
        
      })
      .catch((err) => console.log(err));
  };

  useEffect(() => {
    fetchProfile();
    return () => {
      fetchProfile();
    };
  }, []);

  return (
    <div>
      <Layout>
      <form onSubmit={edit}>
              <h3 className="mb-3 text-center">Update</h3>
              <div class="form-floating mb-2">
                <input
                  type="text"
                  class="form-control shadow-sm"
                  placeholder="First Name"
                  value={fName}
                  onChange={(e) => setFname(e.target.value)}
                  required
                />
                <label for="floatingPassword">First Name</label>
              </div>
              <div class="form-floating mb-2">
                <input
                  type="text"
                  class="form-control shadow-sm"
                  placeholder="Last Name"
                  value={lName}
                  onChange={(e) => setLname(e.target.value)}
                  required
                />
                <label for="floatingPassword">Last Name</label>
              </div>
              <div class="form-floating mb-2">
                <input
                  type="email"
                  class="form-control shadow-sm"
                  placeholder="Email"
                  value={email}
                  onChange={(e) => setEmail(e.target.value)}
                  required
                />
                <label for="floatingPassword">Email</label>
              </div>
              <div class="form-floating mb-2">
                <input
                  type="text"
                  class="form-control shadow-sm"
                  placeholder="Username"
                  value={username}
                  onChange={(e) => setUserName(e.target.value)}
                  required
                />
                <label for="floatingPassword">Username</label>
              </div>
              <div class="form-check form-switch mb-2">
                <label class="form-check-label" for="flexSwitchCheckChecked">
                  Is Active?
                </label>
                <input
                  class="form-check-input"
                  type="checkbox"
                  id="flexSwitchCheckChecked"
                  checked={isActive === true}
                  onChange={(e) => setIsActive(e.target.checked)}
                  required
                />
              </div>

              <div class="d-grid gap-2">
                <button type="submit" className="btn btn-primary btn-sm shadow-sm" disabled={loading}>
                  {loading ? "Updating..." : "Update"}
                </button>
              </div>
            </form>
      </Layout>
    </div>
  );
}

export default Profile;
