import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import Layout from "../../Components/Layout/Layout";
import { httpGeneral } from "../../Utils/Adapters";
import { setUserSession } from "../../Utils/UserData";

function Share(props) {
  const { id } = useParams();
  const [users, setUsers] = useState([]);
  const [shareId, setShareId] = useState("");

  const fetchUsers = () => {
    httpGeneral
      .get("api/user")
      .then((res) => {
        // console.log(res.data);
        setUsers(res.data.users);
      })
      .catch((err) => console.log(err));
  };

  useEffect(() => {
    fetchUsers();
    return () => {
      fetchUsers();
    };
  }, []);

  const handleSubmit = (e) => {
    e.preventDefault();
    const data = {
      note: id,
      user: shareId,
    };
    httpGeneral
      .post("api/share", data)
      .then((res) => {
        // console.log(res.data);
        props.history.push("/home");
      })
      .catch((err) => console.log(err));
  };
  return (
    <div>
      <Layout>
        <div className="row">
          <div className="col-md-8 col-sm-12 m-auto">
            <select
              class="form-select form-select-lg mb-3"
              aria-label=".form-select-lg example"
              onChange={(e) => setShareId(e.target.value)}
            >
              <option selected>Select user to share</option>
              {users.map((d) => (
                <option value={d.id}>{d.username}</option>
              ))}
            </select>
            <button className="btn btn-primary shadow-sm mt-2" onClick={handleSubmit}>
              Share
            </button>
          </div>
        </div>
      </Layout>
    </div>
  );
}

export default Share;
