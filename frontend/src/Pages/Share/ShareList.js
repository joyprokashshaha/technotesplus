import moment from "moment";
import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import Layout from "../../Components/Layout/Layout";
import { httpGeneral } from "../../Utils/Adapters";
import { getUser } from "../../Utils/UserData";

function ShareList() {
  const [notes, setNotes] = useState([]);

  const id = getUser();

  const fetchNote = () => {
    httpGeneral
      .get(`api/share/${id.userid}`)
      .then((res) => {
        // console.log(res.data);
        setNotes(res.data);
      })
      .catch((err) => console.log(err));
  };

  useEffect(() => {
    fetchNote();
    return () => {
      fetchNote();
    };
  }, []);

  const handleDlt = (id) => {
    httpGeneral
      .delete(`api/note/${id}`)
      .then((res) => {
        console.log(res.data);
        fetchNote();
      })
      .catch((err) => console.log(err));
  };
  return (
    <div>
      <Layout>
        <div className="row">
          {notes.map((d) => (
            <div className="col-sm-12 col-md-4 col-lg-4">
              <div className="card shadow mb-4" key={d.id}>
                <div className="card-body">
                  <>
                    <small className="text-muted m-0 " style={{ fontSize: "12px" }}>
                      {moment(d.created_date).startOf("hour").fromNow()}
                    </small>
                    <h5 className="mb-2">{d.note}</h5>
                  </>
                </div>
              </div>
            </div>
          ))}
        </div>
      </Layout>
    </div>
  );
}

export default ShareList;
