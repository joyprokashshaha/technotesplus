import React, { useState } from "react";
import { Link, useHistory } from "react-router-dom";
import { httpLogin } from "../../Utils/Adapters";
import { setUserSession } from "../../Utils/UserData";

function Login(props) {
  const { history } = useHistory();
  const [loading, setLoading] = useState(false);
  const [username, setUserName] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState("");

  const handleSubmit = (e) => {
    e.preventDefault();
    const data = {
      username: username,
      password: password,
    };
    setLoading(true);
    httpLogin
      .post("api/login", data)
      .then((res) => {
        // console.log(res.data);
        setUserSession(res.data.token, res.data);
        if (res.data.statuscode === 200) {
          props.history.push("/home");
        }
        setLoading(false);
      })
      .catch((err) => console.log(err));
    setLoading(false);
    setUserName("");
    setPassword("");
  };

  return (
    <div className="container d-flex justify-content-center align-items-center" style={{ height: "100vh" }}>
      <div className="card shadow">
        <div className="card-body">
          <div className="row m-auto">
            <form onSubmit={handleSubmit}>
              <h3 className="mb-3 text-center">Login</h3>
              <div class="form-floating mb-2">
                <input
                  type="text"
                  class="form-control shadow-sm"
                  id="floatingPassword"
                  placeholder="Username"
                  value={username}
                  onChange={(e) => setUserName(e.target.value)}
                  required
                />
                <label for="floatingPassword">Username</label>
              </div>
              <div class="form-floating mb-2">
                <input
                  type="password"
                  class="form-control shadow-sm"
                  id="floatingPassword"
                  placeholder="Password"
                  value={password}
                  onChange={(e) => setPassword(e.target.value)}
                  required
                />
                <label for="floatingPassword">Password</label>
              </div>
              <div class="d-grid gap-2">
                <button type="submit" className="btn btn-primary btn-sm shadow-sm" disabled={loading}>
                  {loading ? "Logging in..." : "Login"}
                </button>
              </div>
              <small className="mt-2">
                Dont have and account? <Link to="/reg">Register</Link>
              </small>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Login;
