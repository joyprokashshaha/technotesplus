// set the token and user from the session storage
export const setUserSession = (token, user) => {
  localStorage.setItem("token", token);
  localStorage.setItem("user", JSON.stringify(user));
};

// return the token from the session storage
export const getToken = () => {
  return localStorage.getItem("token") || null;
};

//Set URLS
export const setUrls = (url) => {
  return localStorage.setItem("url", url);
};

// Get urls
export const getUrls = () => {
  return localStorage.getItem("url");
};

// return the user data from the session storage
export const getUser = () => {
  const userStr = localStorage.getItem("user");
  if (userStr) return JSON.parse(userStr);
  else return null;
};

// remove the token and user from the session storage
export const removeUserSession = () => {
  localStorage.removeItem("token");
  localStorage.removeItem("user");
  localStorage.removeItem("url");
};
