import axios from "axios";
import { getToken } from "./UserData";

export const httpGeneral = axios.create({
  baseURL: "http://localhost:8000/",
  // headers: { Authorization: "Bearer " + getToken() },
});
httpGeneral.interceptors.request.use(
  (config) => {
    // Do something before request is sent
    config.headers["Authorization"] = "Bearer " + getToken();
    return config;
  },
  (error) => {
    Promise.reject(error);
  }
);
export const httpLogin = axios.create({
  baseURL: "http://localhost:8000/",
});

//Handling Error
httpLogin.interceptors.response.use(
  (response) => {
    return response;
  },
  (error) => {
    if (!error.response) {
      console.log("Please Check Your Internet Connection");
    }

    return Promise.reject(error);
  }
);
