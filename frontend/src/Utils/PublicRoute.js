import React from "react";
import { Route, Redirect } from "react-router-dom";
import { getToken } from "./UserData";

// handle the public routes
function PublicRoute({ component: Component, ...rest }) {
  return (
    <div>
      <Route
        {...rest}
        render={(props) => (!getToken() ? <Component {...props} /> : <Redirect to={{ pathname: "/home" }} />)}
      />
    </div>
  );
}

export default PublicRoute;
