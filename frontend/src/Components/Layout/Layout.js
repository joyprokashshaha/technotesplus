import React from "react";
import { Link } from "react-router-dom";
import { httpGeneral } from "../../Utils/Adapters";
import { getUser, removeUserSession } from "../../Utils/UserData";

function Layout(props) {
  const user = getUser();

  const handleLogout = (e) => {
    e.preventDefault();
    console.log(user.userid);
    httpGeneral
      .post("api/logout", { id: user.userid })
      .then((res) => {
        console.log(res.data);
        removeUserSession();
        window.location.reload();
      })
      .then((err) => console.log(err));
  };
  return (
    <div>
      <nav className="navbar navbar-expand-lg navbar-light bg-light shadow-sm">
        <div className="container">
          <Link to="/home" style={{ fontWeight: "700", color: "#666" }}>
            TechNotePlus
          </Link>
          <div className="justify-content-end" id="navbarNavAltMarkup">
            <div className="navbar-nav d-flex align-items-center">
              <Link to="/tags" style={{ fontWeight: "600", color: "#666", marginRight: "12px" }}>
                Tags
              </Link>
              <Link to="/share" style={{ fontWeight: "600", color: "#666", marginRight: "12px" }}>
                Shared 
              </Link>
              <Link to="/profile" style={{ fontWeight: "600", color: "#666", marginRight: "12px" }}>
                Profile
              </Link>
              <Link className="btn btn-alert btn-sm shadow-sm" to="/changepassword" style={{ marginRight: "12px" }}>
                Change Password
              </Link>
              <button className="btn btn-danger btn-sm shadow-sm" style={{ marginRight: "12px" }} onClick={handleLogout}>
                Logout
              </button>
              
            </div>
          </div>
        </div>
      </nav>
      <div className="container bg-white mt-5 mb-5">{props.children}</div>
    </div>
  );
}

export default Layout;
