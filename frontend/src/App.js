import { BrowserRouter, Route, Router, Switch } from "react-router-dom";
import Err from "./Pages/404/404";
import Home from "./Pages/Home/Home";
import Login from "./Pages/Login/Login";
import NoteAdd from "./Pages/Notes/NoteAdd";
import NoteDetails from "./Pages/Notes/NoteDetails";
import NoteEdit from "./Pages/Notes/NoteEdit";
import Password from "./Pages/password/changepassword";
import Profile from "./Pages/Profile/Profile";
import Registration from "./Pages/Registration/Registration";
import Share from "./Pages/Share/Share";
import ShareList from "./Pages/Share/ShareList";
import Tags from "./Pages/Tags/Tags";
import TagsList from "./Pages/Tags/TagsList";
import PrivateRoute from "./Utils/PrivateRoute";
import PublicRoute from "./Utils/PublicRoute";

function App() {
  return (
    <div>
      <BrowserRouter>
        <Switch>
          <PublicRoute path="/" component={Login} exact />
          <PublicRoute path="/reg" component={Registration} exact />
          <PrivateRoute path="/home" component={Home} exact />
          <PrivateRoute path="/note/add" component={NoteAdd} exact />
          <PrivateRoute path="/notes/edit/:id" component={NoteEdit} exact />
          <PrivateRoute path="/note/:id" component={NoteDetails} exact />
          <PrivateRoute path="/share" component={ShareList} exact />
          <PrivateRoute path="/share/:id" component={Share} exact />
          <PrivateRoute path="/tags" component={TagsList} exact />
          <PrivateRoute path="/tag/add" component={Tags} exact />
          <PrivateRoute path="/profile" component={Profile} exact />
          <PrivateRoute path="/changepassword" component={Password} exact />
          <PrivateRoute component={Err} exact />
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
