## What is this repository for? ##

* TechNotePlus for note created based on user and share the notes and throwing notification against shared note until viewed the note.
* Created API Endpoint to use both for mobile and web.

## How do I get set up? ##

#### Set up backend (Django and Django Rest Framework) ####

-- After cloning the project, navigate to backend directory by giving below command:

*  cd technotesplus/backend

-- Create a virtual environment using python in the above directory:

*  python3 -m venv env

-- After creating an environment run these commands on terminal in same directory to activate environment:

- If linux:

  * source env/bin/activate

- if windows:

  * env\Scripts\activate

-- Run the below command after activation environment:

*  pip install -r requirements.txt

*  python manage.py makemigations

*  python manage.py migrate

*  python manage.py createsuperuser

#### Note: #### 
- if you will get any issues while migration, just delete the all files under migration folder from every django app accept '__init__.py' file.
- And also comment the "notify_user(repeat=Task.HOURLY)" function in project folder url.py file. 
- Then run the migration commands and comment out the function again and run the project.

-- To run the technoteplus project use development phase run or use web service gateway

    *  python manage.py runserver 0.0.0.0:8000

    or

    * gunicorn technoteplus.wsgi --bind 0.0.0.0:8000
	
-- For Email notification used simple lightweight scheduler(background task of django), to run use below command 

    (The scheduler will work hourly basis)

* python manage.py process_tasks

#### Set up frontend (Using React Js) ####

-- Again go to the project directory and navigate to frontend directory by giving below command:

* cd technotesplus/frontend

-- simply just run below command and use the project

* npm install
* npm start


    (disclaimer: 
        
        * frontend running port 'http://localhost:3000/'

        * backend running port 'http://127.0.0.1:8000/'
    )


### Contribution guidelines ###

*  [API Documentation of TechNotePlus](https://documenter.getpostman.com/view/3223227/U16gRTbC)


#### DATABASE ####

-- I have used postgres and sqlite3 both in this project, need to switch the database schema from project directory 'settings.py' file.

-- I am currently working with sqlite3 with migration and share the db.sqlite3 for using without migration.

-- If you need to use postgres use delete migrate files and re-migrate the DB.

* sqlite3 user: admin

* sqlite3 password: admin

